import subprocess
import time
import os

xephyr = subprocess.Popen(['Xephyr', ':1', '-screen', '1024x768'])
time.sleep(0.1)
os.environ['DISPLAY'] = ':1'
subprocess.check_call(['cabal', 'build'])
exe = subprocess.check_output(['cabal', 'list-bin', 'T21708'], encoding='UTF-8').strip()
xmonad = subprocess.Popen(['rr', 'record', '-o', './rr-out', exe, '+RTS', '-N8', '-DS'], env={'DISPLAY': ':1'})
n = 0

while True:
    n += 1
    print(n)
    procs = []
    for i in range(10):
        procs.append(subprocess.Popen(['gnome-calculator']))
        time.sleep(0.1)

    time.sleep(1)
    for p in procs:
        p.kill()

